import React from "react";

function SleepButton(props) {
  return (
    <button
      style={{
        backgroundColor: "#000",
        color: "#fff",
        border: "1px solid #fff",
        fontFamily: "VT323, monospace"
      }}
      onClick={props.onClickSleep}
    >
      Sleep
    </button>
  );
}

export default SleepButton;
