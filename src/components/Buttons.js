import React from "react";
import FeedButton from "./FeedButton";
import PlayButton from "./PlayButton";
import HealButton from "./HealButton";
import SleepButton from "./SleepButton";
import styled from "styled-components";

function Buttons(props) {
  function feed() {
    props.feed();
  }
  function play() {
    props.play();
  }
  function sleep() {
    props.sleep();
  }
  function heal() {
    props.heal();
  }

  const Block = styled.div`
    width: 100vw;
    justify-content: space-around;
    display: flex;
    position: absolute;
    bottom: 100px;
  `;

  return (
    <Block>
      <HealButton onClickHeal={heal} />
      <SleepButton onClickSleep={sleep} />
      <FeedButton onClickFeed={feed} />
      <PlayButton onClickPlay={play} />
    </Block>
  );
}

export default Buttons;
