import React, { Component } from "react";
import styled from "styled-components";

class CreatureImage extends Component {
  render() {
    const Creature = styled.div`
      display: flex;
      justify-content: center;
      align-item: center;
      position: absolute;
      top: 40%;
      width: 100%;
    `;
    return (
      <Creature>
        <img
          alt="creature"
          style={{ backgroundColor: "#fff", padding: "30px" }}
          src={this.props.form}
        />
      </Creature>
    );
  }
}

export default CreatureImage;
