import React from "react";

function HealButton(props) {
  return (
    <button
      style={{
        backgroundColor: "#000",
        color: "#fff",
        border: "1px solid #fff",
        fontFamily: "VT323, monospace"
      }}
      onClick={props.onClickHeal}
    >
      {" "}
      Heal
    </button>
  );
}

export default HealButton;
