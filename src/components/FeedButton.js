import React from "react";

function FeedButton(props) {
  return (
    <button
      style={{
        backgroundColor: "#000",
        color: "#fff",
        border: "1px solid #fff",
        fontFamily: "VT323, monospace"
      }}
      onClick={props.onClickFeed}
    >
      Feed
    </button>
  );
}

export default FeedButton;
