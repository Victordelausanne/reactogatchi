import React from "react";
import styled from "styled-components";

function Title(props) {
  // const [textTitle, setState] =
  // useState('Tamagotchi')

  //   props.alive ? setState('Tamagotchi') : setState('Game')

  const TitleCtnr = styled.div`
    width: 100vw;
    text-align: center;
    color: #fff;
    justify-content: space-around;
  `;

  return (
    <TitleCtnr>
      <h1 style={{ margin: "0", padding: "0" }}> Tamagotchi </h1>
    </TitleCtnr>
  );
}

export default Title;
