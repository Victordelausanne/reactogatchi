import React, { Component } from "react";
import styled from "styled-components";

class Stats extends Component {
  render() {
    const StatsCtnr = styled.div`
      width: 100vw;
      display: flex;
      justify-content: space-around;
      position: absolute;
      bottom: 50px;
    `;

    return (
      <StatsCtnr>
        <div
          id="health"
          style={{ height: "20px", width: "20%", border: "1px solid #fff" }}
        >
          <div
            className="bar"
            style={{
              height: "100%",
              width: this.props.health + "%",
              backgroundColor: "#fff"
            }}
          >
            Health
          </div>
        </div>
        <div
          id="energy"
          style={{ height: "20px", width: "20%", border: "1px solid #fff" }}
        >
          <div
            className="bar"
            style={{
              height: "100%",
              width: this.props.energy + "%",
              backgroundColor: "#fff"
            }}
          >
            Energy
          </div>
        </div>
        <div
          id="hunger"
          style={{ height: "20px", width: "20%", border: "1px solid #fff" }}
        >
          <div
            className="bar"
            style={{
              height: "100%",
              width: this.props.hunger + "%",
              backgroundColor: "#fff"
            }}
          >
            Hunger
          </div>
        </div>
        <div
          id="happiness"
          style={{ height: "20px", width: "20%", border: "1px solid #fff" }}
        >
          <div
            className="bar"
            style={{
              height: "100%",
              width: this.props.happiness + "%",
              backgroundColor: "#fff"
            }}
          >
            Happiness
          </div>
        </div>
      </StatsCtnr>
    );
  }
}

export default Stats;
