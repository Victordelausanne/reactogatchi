import React from "react";

function PlayButton(props) {
  return (
    <button
      style={{
        backgroundColor: "#000",
        color: "#fff",
        border: "1px solid #fff",
        fontFamily: "VT323, monospace"
      }}
      onClick={props.onClickPlay}
    >
      Play
    </button>
  );
}

export default PlayButton;
