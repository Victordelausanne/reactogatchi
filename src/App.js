import React, { Component } from "react";
import Buttons from "./components/Buttons";
import Stats from "./components/Stats";
import CreatureImage from "./components/CreatureImage";
import styled from "styled-components";
import Title from "./components/Title";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: "baby",
      health: 100,
      alive: true,
      hunger: 100,
      energy: 100,
      happiness: 100,
      createdTime: 0,
      level: 1
    };
    this.feed = this.feed.bind(this);
    this.play = this.play.bind(this);
    this.sleep = this.sleep.bind(this);
    this.heal = this.heal.bind(this);
    this.resetStats = this.resetStats.bind(this);
  }

  resetStats() {
    this.setState({
      health: 100,
      hunger: 100,
      happiness: 100,
      energy: 100,
      level: 1,
      alive: true
    });
    localStorage.setItem("exist", 1);
    localStorage.setItem("health", 100);
    localStorage.setItem("energy", 100);
    localStorage.setItem("happiness", 100);
    localStorage.setItem("hunger", 100);
    localStorage.setItem("level", 1);
  }

  setForm() {
    if (this.state.level < 99) {
      this.setState({ form: "https://zupimages.net/up/19/21/al8c.png" });
    }
    if (this.state.level > 99) {
      this.setState({ form: "https://i.imgur.com/RAKvjBV.png" });
    }
    if (this.state.level > 199) {
      this.setState({ form: "https://i.imgur.com/i1sOCp7.png" });
    }
    if (this.state.level > 299) {
      this.setState({ form: "https://i.imgur.com/ngsiG3o.png" });
    }
    if (this.state.level > 399) {
      this.setState({ form: "https://i.imgur.com/CkX05sY.png" });
    }
    if (this.state.level > 499) {
      this.setState({ form: "https://i.imgur.com/CkX05sY.png" });
    }
    if (this.state.level > 599) {
      this.setState({ form: "https://i.imgur.com/q0flIg4.png" });
    }
    if (this.state.level > 699) {
      this.setState({ form: "https://i.imgur.com/OZ0d5vP.png" });
    }
    if (this.state.level > 799) {
      this.setState({ form: "https://i.imgur.com/pkmgkm7.png" });
    }
    if (this.state.level > 899) {
      this.setState({ form: "https://i.imgur.com/5OYAM4u.png" });
    }
    if (this.state.level > 999) {
      this.setState({ form: "https://i.imgur.com/yTDglkC.png" });
    }
    if (this.state.level > 1099) {
      this.setState({ form: "https://i.imgur.com/rh4ldAK.png" });
    }
    if (this.state.alive === false) {
      this.setState({ form: "https://i.imgur.com/xTGpUkf.png" });
    }
  }

  setTime() {
    const date = Date.now();
    localStorage.setItem("time", date);
  }

  checkTime() {
    const lastSave = localStorage.getItem("time");
    const actualDate = Date.now();
    if (lastSave !== null) {
      let updateStat = actualDate - lastSave;
      updateStat = Math.round(updateStat / 10000);
      if (updateStat > 3) {
        console.log("check ", updateStat);
        this.setStats();
        this.setState({
          health: this.state.health - updateStat,
          hunger: this.state.hunger - updateStat,
          energy: this.state.energy - updateStat,
          happiness: this.state.happiness - updateStat,
          level: this.state.level + updateStat
        });
      }
    }
  }

  saveStats() {
    localStorage.setItem("exist", 1);
    localStorage.setItem("health", this.state.health);
    localStorage.setItem("energy", this.state.energy);
    localStorage.setItem("happiness", this.state.happiness);
    localStorage.setItem("hunger", this.state.hunger);
    localStorage.setItem("level", this.state.level);
  }

  feed() {
    let hunger = this.state.hunger + 10;
    if (hunger > 100) {
      hunger = 100;
    }
    this.saveStats();
    this.setState({ hunger: hunger });
  }

  play() {
    let happiness = this.state.happiness + 10;
    if (happiness > 100) {
      happiness = 100;
    }
    this.saveStats();
    this.setState({ happiness: happiness });
  }

  sleep() {
    let energy = this.state.energy + 10;
    if (energy > 100) {
      energy = 100;
    }
    this.saveStats();
    this.setState({ energy: energy });
  }

  heal() {
    let health = this.state.health + 10;
    if (health > 100) {
      health = 100;
    }
    this.saveStats();
    this.setState({ health: health });
  }

  setStats() {
    let existing = localStorage.getItem("exist");
    let health = localStorage.getItem("health");
    let hunger = localStorage.getItem("hunger");
    let energy = localStorage.getItem("energy");
    let happiness = localStorage.getItem("happiness");
    let level = localStorage.getItem("level");
    level = parseInt(level);
    if (existing !== null) {
      this.setState({
        health: health,
        hunger: hunger,
        energy: energy,
        happiness: happiness,
        level: level
      });
    }
  }

  decreasingStats() {
    let health = this.state.health;
    let hunger = this.state.hunger;
    let energy = this.state.energy;
    let happiness = this.state.happiness;
    let level = this.state.level;
    if (health > 0 && hunger > 0 && energy > 0 && happiness > 0) {
      this.setState({
        health: health - 1,
        hunger: hunger - 3,
        energy: energy - 2,
        happiness: happiness - 4,
        level: level + 1
      });
    } else {
      this.setState({
        form: "https://i.imgur.com/xTGpUkf.png",
        alive: false,
        health: 0,
        energy: 0,
        happiness: 0,
        level: 0
      });
    }
  }

  decreasingHealth() {
    if (this.state.health > 0) {
      if (this.state.hunger < 30 || this.state.energy < 30) {
        let health = this.state.health - 2;
        this.setState({ health: health });
      }
    }
  }

  componentDidMount() {
    this.setStats();
    this.saveStats();
    this.checkTime();
    this.setForm();
    setInterval(() => {
      this.setForm();
      this.decreasingStats();
      this.decreasingHealth();
      this.setTime();
      this.saveStats();
    }, 10000);
  }

  render() {
    const Level = styled.div`
      width: 100vw;
      text-align: center;
      color: #fff;
      padding-top: 50px;
    `;
    const App = styled.div`
      background-color: #000;
      width: 100vw;
      height: 100vh;
      font-family: "VT323", monospace;
    `;
    const Reset = styled.div`
      width: 100vw;
      display: flex;
      justify-content: center;
      margin-top: 20px;
      font-family: "VT323", monospace;
    `;

    return (
      <App>
        <Title alive={this.state.alive} level={this.state.level} />
        <Level> level: {this.state.level}</Level>
        <CreatureImage form={this.state.form} />
        <Buttons
          alive={this.state.alive}
          form={this.state.form}
          feed={this.feed}
          play={this.play}
          sleep={this.sleep}
          heal={this.heal}
        />
        <Stats
          hunger={this.state.hunger}
          energy={this.state.energy}
          health={this.state.health}
          happiness={this.state.happiness}
          level={this.state.level}
        />
        <Reset>
          <button
            style={{
              backgroundColor: "#000",
              color: "#fff",
              border: "1px solid #fff",
              fontFamily: "VT323, monospace"
            }}
            onClick={this.resetStats}
          >
            {" "}
            Recommencer{" "}
          </button>
        </Reset>
      </App>
    );
  }
}
export default App;
